class Arbol {
    constructor(dato){
        this.root = null;
    }

    agregarNodo(dato){
        var nuevoNodo = new Nodo(dato);

        if(this.root === null){
            this.root = nuevoNodo;
        }else{
            this.agregarNodoP(this.root, nuevoNodo);
        }
    }

    agregarNodoP(nodo, nuevoNodo){
        if(nuevoNodo.dato < nodo.dato){
            if(nodo.izquierda === null){
                nodo.izquierda = nuevoNodo;
            }else{
                this.agregarNodoP(nodo.izquierda, nuevoNodo);
            }
        }else{
            if(Node.derecha === null) nodo.derecha = nuevoNodo;
            else this.agregarNodoP(nodo.derecha, nuevoNodo);
        }
    }

    enOrden(nodo){
        if(nodo !== null){
            this.enOrden(nodo.izquierda);
            console.log(nodo.dato);
            this.enOrden(nodo.derecha);
        }
    }

}

module.exports = {Arbol};